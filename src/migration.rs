use surrealdb::{engine::local::Db, Surreal};

const USERS_TABLE: &str = r#"
DEFINE TABLE user SCHEMALESS;
DEFINE FIELD username ON TABLE user TYPE string;
DEFINE FIELD password ON TABLE user TYPE string;
DEFINE INDEX usernameIndex ON TABLE user COLUMNS username UNIQUE;
"#;

const FILES_TABLE: &str = r#"
DEFINE TABLE file SCHEMALESS;
DEFINE FIELD name ON TABLE file TYPE string;
DEFINE FIELD date ON TABLE file TYPE datetime;
DEFINE FIELD size ON TABLE file TYPE string;
DEFINE FIELD downloads ON TABLE file TYPE number;
DEFINE INDEX filenameIndex ON TABLE file COLUMNS name UNIQUE;
"#;

const SHARE_TABLE: &str = r#"
DEFINE TABLE share SCHEMALESS;
DEFINE FIELD name ON TABLE share TYPE string;
DEFINE FIELD date ON TABLE share TYPE datetime;
DEFINE INDEX sharenameIndex ON TABLE share COLUMNS name UNIQUE;
"#;

const ALIAS_TABLE: &str = r#"
DEFINE TABLE alias SCHEMALESS;
DEFINE FIELD file ON TABLE alias TYPE string;
DEFINE INDEX aliasfileIndex ON TABLE alias COLUMNS file UNIQUE;
"#;

pub async fn run(db: &Surreal<Db>) -> Result<(), surrealdb::Error> {
    db.query(USERS_TABLE).await?;
    db.query(FILES_TABLE).await?;
    db.query(SHARE_TABLE).await?;
    db.query(ALIAS_TABLE).await?;
    Ok(())
}
