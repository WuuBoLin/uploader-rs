use std::env;

use axum::Router;
use axum_login::{
    tower_sessions::{ExpiredDeletion, Expiry, SessionManagerLayer},
    AuthManagerLayerBuilder,
};
use surrealdb::{
    engine::local::{Db, RocksDb},
    Surreal,
};
use time::Duration;
use tower_http::{
    classify::StatusInRangeAsFailures,
    compression::{predicate::NotForContentType, CompressionLayer, DefaultPredicate, Predicate},
    services::ServeDir,
    trace::TraceLayer,
};
use tower_sessions_surrealdb_store::SurrealSessionStore;
use tracing::info;

use crate::{
    auth::AuthBackend,
    migration,
    routes::{self},
};

#[derive(Debug, Clone)]
pub struct ServerState {
    pub db: Surreal<Db>,
    pub s3: aws_sdk_s3::Client,
}

pub struct Server;

impl Server {
    pub async fn launch() -> anyhow::Result<()> {
        // Database
        let db = surrealdb::Surreal::new::<RocksDb>(std::env::var("DATABASE_PATH")?).await?;
        db.use_ns("dashthings").use_db("dashthings").await?;
        migration::run(&db).await?;

        db.query("INSERT INTO user (username, password) VALUES ('admin', $password) ON DUPLICATE KEY UPDATE password = $password")
            .bind(("password", password_auth::generate_hash(env::var("ADMIN_PASSWORD")?)))
            .await?;

        // Sessionlayer
        let session_store = SurrealSessionStore::new(db.clone(), "session".to_string());
        let expired_session_cleanup_interval: u64 = 1;
        tokio::task::spawn(session_store.clone().continuously_delete_expired(
            tokio::time::Duration::from_secs(60 * expired_session_cleanup_interval),
        ));
        let session_layer = SessionManagerLayer::new(session_store)
            .with_secure(false)
            .with_expiry(Expiry::OnInactivity(Duration::hours(1)));

        // Authlayer
        let auth_backend = AuthBackend::new(db.clone());
        let auth_layer = AuthManagerLayerBuilder::new(auth_backend, session_layer).build();

        // Compressionlayer
        let compression_layer = CompressionLayer::new()
            .br(true)
            .compress_when(DefaultPredicate::new().and(NotForContentType::new("application/json")));

        // S3 Bucket
        let sdk_config = aws_config::load_from_env().await;
        let s3 = aws_sdk_s3::Client::new(&sdk_config);

        // State
        let state = ServerState { db, s3 };

        // Router
        let router = Router::new()
            .nest("/", routes::guest_router())
            .nest("/", routes::quick_router())
            .nest("/api", routes::api_router())
            .nest("/auth", routes::auth_router())
            .nest("/admin", routes::admin_router())
            .nest_service("/static", ServeDir::new("static"))
            .layer(auth_layer)
            .layer(compression_layer)
            .layer(TraceLayer::new(
                StatusInRangeAsFailures::new(405..=599).into_make_classifier(),
            ))
            .with_state(state);

        // Server
        let port = std::env::var("PORT").unwrap_or_else(|_| "80".to_string());
        let listener = tokio::net::TcpListener::bind(format!("0.0.0.0:{}", port)).await?;

        info!("Server started on port {port}");

        axum::serve(listener, router).await?;

        Ok(())
    }
}
