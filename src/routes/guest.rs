use askama::Template;
use axum::{
    extract::{Host, Path, Query, State},
    http::{HeaderMap, StatusCode},
    response::{Html, IntoResponse, Redirect, Response},
};
use mime_guess::mime;
use serde::{Deserialize, Serialize};
use surrealdb::sql::Thing;

use crate::{
    bucket::{self, get_presigned_url, UrlType::*},
    server::ServerState,
    templates::{FileMeta, IndexTemplate, LinkTemplate, NotFoundTemplate, PdfViewTemplate},
};

use super::api::{DisplayQuery, File};

pub async fn index_page(State(state): State<ServerState>) -> Result<Html<String>, StatusCode> {
    let files: Vec<File> = state.db
	.query("SELECT *, (SELECT date FROM share WHERE name = $parent.name)[0].date AS sharedate FROM file WHERE name IN (SELECT name FROM share).name ORDER BY sharedate DESC")
	.await
	.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
	.take(0)
	.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let files = files
        .iter()
        .map(|f| FileMeta {
            name: f.name.clone(),
            date: f.date.to_rfc3339(),
            size: f.size.clone(),
            sharedate: f.sharedate.unwrap().to_rfc3339(),
            downloads: f.downloads,
        })
        .collect();

    Ok(Html(IndexTemplate { all_files: files }.render().unwrap()))
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Alias {
    pub id: Option<Thing>,
    pub file: String,
}

pub async fn link(
    // auth: AuthSession,
    headers: HeaderMap,
    Host(host): Host,
    Path(link): Path<String>,
    Query(query): Query<DisplayQuery>,
    State(state): State<ServerState>,
) -> Result<Response, (StatusCode, Html<String>)> {
    let Some(alias) = state
        .db
        .select::<Option<Alias>>(("alias", &link))
        .await
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
    else {
        return Err((
            StatusCode::NOT_FOUND,
            Html(NotFoundTemplate.render().unwrap()),
        ));
    };

    // if state
    //     .db
    //     .query("SELECT * FROM share WHERE name = $name")
    //     .bind(("name", &alias.file))
    //     .await
    //     .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
    //     .take::<Option<Share>>(0)
    //     .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
    //     .is_none()
    //     && auth.user.is_none()
    // {
    //     return Ok(
    //         Redirect::temporary(format!("/auth/login?next=/{}", link).as_str()).into_response(),
    //     );
    // }

    let is_display = query.display.unwrap_or_default();

    let is_browser = headers
        .get("User-Agent")
        .map(|header_value| {
            header_value
                .to_str()
                .unwrap_or_default()
                .contains("Mozilla")
        })
        .unwrap_or_default();

    let is_image = mime_guess::from_ext(alias.file.split('.').last().unwrap_or(&alias.file))
        .first_or_octet_stream()
        .type_()
        .eq(&mime::IMAGE);

    let presigned_uri = if is_display || !is_browser || is_image {
        get_presigned_url(
            bucket::PresignedMethod::Get(Direct),
            &state.s3,
            &alias.file,
            3600,
        )
        .await
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
        .uri()
        .to_owned()
    } else {
        String::new()
    };

    if is_display || !is_browser {
        return Ok(Redirect::permanent(&presigned_uri).into_response());
    }

    let url = format!("https://{}/{}", host.as_str(), link);

    let Some(size) = state
        .db
        .query("(SELECT size FROM file WHERE name = $name).size")
        .bind(("name", &alias.file))
        .await
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
        .take::<Option<String>>(0)
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
    else {
        return Err((
            StatusCode::INTERNAL_SERVER_ERROR,
            Html(NotFoundTemplate.render().unwrap()),
        ));
    };

    Ok(Html(
        LinkTemplate {
            filename: alias.file,
            url,
            size,
            content_url: presigned_uri,
        }
        .render()
        .unwrap(),
    )
    .into_response())
}

pub async fn pdf() -> Html<String> {
    Html(PdfViewTemplate.render().unwrap())
}
