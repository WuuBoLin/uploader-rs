pub mod admin;
pub mod api;
pub mod auth;
pub mod guest;

use axum::{
    extract::State,
    http::{header, HeaderValue, StatusCode},
    routing::{get, post},
    Router,
};
use axum_login::login_required;

use tower_http::set_header::SetResponseHeaderLayer;
use tracing::info;

use crate::{auth::AuthBackend, server::ServerState};

use self::{
    admin::{admin_page, quick_token},
    api::{
        callback, delete_file, delete_files, file, modify_file, modify_files, presign_download,
        presign_upload,
    },
    auth::{login, login_page, logout},
    guest::{index_page, link, pdf},
};

pub fn auth_router() -> Router<ServerState> {
    Router::new()
        .route("/login", get(login_page).post(login))
        .route("/logout", get(logout))
        .layer(SetResponseHeaderLayer::if_not_present(
            header::CACHE_CONTROL,
            HeaderValue::from_static("no-store"),
        ))
}

pub fn api_router() -> Router<ServerState> {
    Router::new()
        .route("/files", post(modify_files).delete(delete_files))
        .route(
            "/files/:file",
            get(file).post(modify_file).delete(delete_file),
        )
        .route("/request/upload", post(presign_upload))
        .route_layer(login_required!(AuthBackend))
        .route("/request/download", post(presign_download))
        .route("/upload/callback", post(callback))
        .layer(SetResponseHeaderLayer::if_not_present(
            header::CONTENT_TYPE,
            HeaderValue::from_static("application/json"),
        ))
        .route("/healthz", get(healthz))
        .layer(SetResponseHeaderLayer::if_not_present(
            header::CACHE_CONTROL,
            HeaderValue::from_static("no-store"),
        ))
}

pub fn guest_router() -> Router<ServerState> {
    Router::new()
        .route("/", get(index_page))
        .route("/:link", get(link))
        .route("/pdfview", get(pdf))
}

pub fn admin_router() -> Router<ServerState> {
    Router::new()
        .route("/", get(admin_page))
        .route_layer(login_required!(AuthBackend, login_url = "/auth/login"))
}

pub fn quick_router() -> Router<ServerState> {
    Router::new()
        .route("/quick/:token", get(quick_token))
        .layer(SetResponseHeaderLayer::if_not_present(
            header::CACHE_CONTROL,
            HeaderValue::from_static("no-store"),
        ))
}

pub async fn healthz(State(state): State<ServerState>) -> Result<StatusCode, StatusCode> {
    info!("PERFORMING HEALTH CHECK...");
    state
        .db
        .health()
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    info!("HEALTH CHECK PASSED");
    Ok(StatusCode::OK)
}
