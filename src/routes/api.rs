use axum::extract::{Path, Query, State};
use axum::http::{HeaderMap, StatusCode};
use axum::response::IntoResponse;

use axum::{debug_handler, Json};

use chrono::{DateTime, Local};
use humansize::{format_size, DECIMAL};
use regex::Regex;
use serde::{Deserialize, Serialize};
use surrealdb::engine::local::Db;
use surrealdb::Surreal;

use std::collections::HashMap;
use std::env;
use surrealdb::error::Db::*;
use surrealdb::sql::{Datetime, Thing};
use tracing::info;

use crate::auth::AuthSession;
use crate::bucket::{
    self,
    UrlType::{self},
};
use crate::server::ServerState;

use super::guest::Alias;

pub async fn file(
    State(state): State<ServerState>,
    Path(file): Path<String>,
) -> Result<Json<FileStateResponse>, StatusCode> {
    let mut data = state
        .db
        .query("SELECT * FROM file WHERE name = $name")
        .query("(SELECT date FROM share WHERE name = $name)[0].date")
        .query("(SELECT meta::id(id) AS id FROM alias WHERE file = $name)[0].id")
        .bind(("name", file))
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let Some(file) = data
        .take::<Option<File>>(0)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
    else {
        return Err(StatusCode::NOT_FOUND);
    };

    let sharedate = data
        .take::<Option<String>>(1)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let link = data
        .take::<Option<String>>(2)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(Json(FileStateResponse {
        name: file.name,
        date: file.date.to_rfc3339(),
        size: file.size,
        downloads: file.downloads,
        sharedate,
        link,
    }))
}

pub async fn modify_file(
    state: State<ServerState>,
    Path(file): Path<String>,
    Json(req): Json<FileModifyRequest>,
) -> Result<StatusCode, (StatusCode, String)> {
    let mut req: FilesModifyRequest = req.into();
    req.files = vec![file];
    modify_files(state, Json(req)).await
}

pub async fn modify_files(
    State(state): State<ServerState>,
    Json(mut req): Json<FilesModifyRequest>,
) -> Result<StatusCode, (StatusCode, String)> {
    let db = &state.db;
    let internal_error = || (StatusCode::INTERNAL_SERVER_ERROR, String::new());

    if req.files.is_empty() || req.share.is_none() && req.name.is_none() && req.link.is_none() {
        return Err((StatusCode::BAD_REQUEST, String::new()));
    }

    if req.files.len() > 1
        && (req.name.is_some()
            || req
                .link
                .as_ref()
                .map(|link| !link.is_empty())
                .unwrap_or_default())
    {
        return Err((
            StatusCode::BAD_REQUEST,
            String::from("MULTIPLE_FILES_NOT_ALLOWED"),
        ));
    }

    if req.name.is_some() {
        let new_name = req.name.as_ref().unwrap();
        let regex = Regex::new(r"^[\w\-. ]+$").unwrap();
        if !regex.is_match(new_name) {
            return Err((StatusCode::BAD_REQUEST, String::from("FILENAME_ILLEGAL")));
        }
    }

    if req
        .link
        .as_ref()
        .map(|link| !link.is_empty())
        .unwrap_or_default()
    {
        let link = req.link.as_ref().unwrap();
        let regex = Regex::new(r"^[a-zA-Z0-9]+$").unwrap();
        if !regex.is_match(link) {
            return Err((StatusCode::BAD_REQUEST, String::from("LINK_ILLEGAL")));
        }
    }

    for file in &req.files {
        if db
            .query("(SELECT name FROM file WHERE name = $name)[0].name")
            .bind(("name", &file))
            .await
            .map_err(|_| internal_error())?
            .take::<Option<String>>(0)
            .map_err(|_| internal_error())?
            .is_none()
        {
            return Err((StatusCode::NOT_FOUND, format!("FILE_NOT_FOUND: {file}")));
        };
    }

    if let Some(new_name) = req.name {
        if db
            .query("(SELECT name FROM file WHERE name = $name)[0].name")
            .bind(("name", &new_name))
            .await
            .map_err(|_| internal_error())?
            .take::<Option<String>>(0)
            .map_err(|_| internal_error())?
            .is_some()
        {
            return Err((StatusCode::BAD_REQUEST, String::from("FILENAME_EXIST")));
        };

        bucket::rename(&state.s3, req.files.first().unwrap(), &new_name)
            .await
            .map_err(|_| internal_error())?;

        db.query("UPDATE file SET name = $new_name WHERE name = $name")
            .query("UPDATE share SET name = $new_name WHERE name = $name")
            .query("UPDATE alias SET file = $new_name WHERE file = $name")
            .bind(("name", &req.files.first().unwrap()))
            .bind(("new_name", &new_name))
            .await
            .map_err(|_| internal_error())?;

        req.files = vec![new_name];
    }

    if let Some(share) = req.share {
        if share {
            let values: Vec<Share> = req
                .files
                .iter()
                .map(|file| Share {
                    id: None,
                    name: file.to_string(),
                    date: Local::now().to_utc().into(),
                })
                .collect();

            db.query("INSERT INTO share $values")
                .bind(("values", values))
                .await
                .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, String::new()))?;
        } else {
            db.query("DELETE share WHERE name IN $names")
                .bind(("names", &req.files))
                .await
                .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, String::new()))?;
        }
    }

    if let Some(link) = req.link {
        if link.is_empty() {
            db.query("DELETE alias WHERE file IN $names")
                .bind(("names", &req.files))
                .await
                .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, String::new()))?;
        } else {
            let create_result = db
                .create::<Option<Alias>>(("alias", &link))
                .content(Alias {
                    id: None,
                    file: req.files.first().unwrap().to_string(),
                })
                .await;
            match create_result {
                Err(surrealdb::Error::Db(IndexExists { .. })) => {
                    let file = req.files.first().unwrap();
                    db.query("DELETE alias WHERE file = $name")
                        .bind(("name", file))
                        .await
                        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, String::new()))?;
                    db.create::<Option<Alias>>(("alias", &link))
                        .content(Alias {
                            id: None,
                            file: file.to_string(),
                        })
                        .await
                        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, String::new()))?;
                }
                Err(surrealdb::Error::Db(RecordExists { .. })) => {
                    return Err((StatusCode::BAD_REQUEST, String::from("LINK_EXIST")))
                }
                Err(_) => return Err((StatusCode::INTERNAL_SERVER_ERROR, String::new())),
                Ok(_) => {}
            }
        }
    }

    Ok(StatusCode::OK)
}

pub async fn delete_file(
    State(state): State<ServerState>,
    Path(file): Path<String>,
) -> Result<StatusCode, StatusCode> {
    bucket::delete(&state.s3, &[&file])
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    _delete_files(&state.db, &vec![file])
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(StatusCode::OK)
}

pub async fn delete_files(
    State(state): State<ServerState>,
    Json(req): Json<DeleteRequest>,
) -> Result<StatusCode, StatusCode> {
    if req.files.is_empty() {
        return Err(StatusCode::BAD_REQUEST);
    }

    bucket::delete(&state.s3, &req.files)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    _delete_files(&state.db, &req.files)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(StatusCode::OK)
}

pub async fn _delete_files(db: &Surreal<Db>, files: &Vec<String>) -> anyhow::Result<()> {
    db.query("DELETE file WHERE name IN $names")
        .query("DELETE share WHERE name IN $names")
        .query("DELETE alias WHERE file IN $names")
        .bind(("names", files))
        .await?;

    Ok(())
}

pub async fn presign_upload(
    State(state): State<ServerState>,
    req: Json<PresignRequest>,
) -> Result<Json<PresignResponse>, StatusCode> {
    if req.files.first().is_none() {
        return Err(StatusCode::BAD_REQUEST);
    }

    let client = state.s3;

    let Ok(presigned) = bucket::get_presigned_url(
        bucket::PresignedMethod::Put,
        &client,
        req.files.first().unwrap(),
        10800,
    )
    .await
    else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    let headers_map: HashMap<String, String> = presigned
        .headers()
        .map(|(k, v)| (k.to_string(), v.to_string()))
        .collect();

    if req.share.unwrap_or_default() {
        state.db.query("INSERT INTO share (name, date) VALUES ($name, <datetime> $date) ON DUPLICATE KEY UPDATE date = <datetime> $date").bind(("name", req.files.first().unwrap())).bind(("date", Local::now())).await.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    } else {
        state
            .db
            .query("DELETE share WHERE name = $name")
            .bind(("name", req.files.first().unwrap()))
            .await
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    }

    return Ok(Json(PresignResponse {
        url: presigned.uri().to_string(),
        headers: headers_map,
        method: presigned.method().to_string(),
    }));
}

#[debug_handler]
pub async fn presign_download(
    auth: AuthSession,
    State(state): State<ServerState>,
    Query(query): Query<DisplayQuery>,
    req: Json<PresignRequest>,
) -> Result<impl IntoResponse, StatusCode> {
    let db = state.db;
    let mut presigned_responses: Vec<PresignResponse> = Vec::with_capacity(req.files.len());
    for file in &req.files {
        if db
            .query("SELECT * FROM type::table($table) WHERE name = $name")
            .bind(("table", "file"))
            .bind(("name", file))
            .await
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
            .take::<Option<File>>(0)
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
            .is_none()
        {
            return Err(StatusCode::NOT_FOUND);
        }

        if db
            .query("SELECT * FROM share WHERE name = $name")
            .bind(("name", file))
            .await
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
            .take::<Option<Share>>(0)
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
            .is_none()
            && auth.user.is_none()
        {
            return Err(StatusCode::FORBIDDEN);
        }

        let url_type = match query.display.unwrap_or_default() {
            true => UrlType::Preview,
            false => UrlType::Direct,
        };

        if matches!(url_type, UrlType::Direct) {
            db.query("UPDATE file SET downloads += 1 WHERE name = $name")
                .bind(("name", file))
                .await
                .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
        }

        let presigned = bucket::get_presigned_url(
            bucket::PresignedMethod::Get(url_type),
            &state.s3,
            file,
            3600,
        )
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

        let headers_map: HashMap<String, String> = presigned
            .headers()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect();

        presigned_responses.push(PresignResponse {
            method: presigned.method().to_string(),
            url: presigned.uri().to_string(),
            headers: headers_map,
        })
    }

    Ok(Json(presigned_responses).into_response())
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CallbackRequest {
    filename: String,
    date: String,
    size: usize,
}

pub async fn callback(
    State(state): State<ServerState>,
    headers: HeaderMap,
    req: Json<CallbackRequest>,
) -> Result<StatusCode, StatusCode> {
    let authorization = format!("Bearer {}", env::var("CALLBACK_TOKEN").unwrap());
    if headers
        .get("Authorization")
        .filter(|value| {
            value
                .to_str()
                .map(|v| v == authorization)
                .unwrap_or_default()
        })
        .is_none()
    {
        return Err(StatusCode::UNAUTHORIZED);
    }

    let file = File {
        id: None,
        name: req.filename.to_string(),
        date: DateTime::parse_from_rfc3339(&req.date)
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
            .to_utc()
            .into(),
        size: format_size(req.size, DECIMAL),
        downloads: 0,
        sharedate: None,
    };

    state.db.query("INSERT INTO file (name, date, size, downloads) VALUES ($name, <datetime> $date, $size, 0) ON DUPLICATE KEY UPDATE date = <datetime> $date, downloads = 0, size = $size").bind(("name", file.name)).bind(("date", file.date)).bind(("size", file.size)).await.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    info!("[CALLBACK] File {} uploaded", req.filename);

    Ok(StatusCode::CREATED)
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct File {
    pub id: Option<Thing>,
    pub name: String,
    pub date: Datetime,
    pub sharedate: Option<DateTime<Local>>,
    pub size: String,
    pub downloads: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FileStateResponse {
    pub name: String,
    pub date: String,
    pub size: String,
    pub downloads: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sharedate: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Share {
    pub id: Option<Thing>,
    pub name: String,
    pub date: Datetime,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PresignRequest {
    files: Vec<String>,
    share: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PresignResponse {
    method: String,
    url: String,
    headers: HashMap<String, String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FileModifyRequest {
    share: Option<bool>,
    name: Option<String>,
    link: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FilesModifyRequest {
    files: Vec<String>,
    share: Option<bool>,
    name: Option<String>,
    link: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeleteRequest {
    files: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DisplayQuery {
    pub display: Option<bool>,
}

impl From<FileModifyRequest> for FilesModifyRequest {
    fn from(val: FileModifyRequest) -> Self {
        FilesModifyRequest {
            files: vec![],
            share: val.share,
            name: val.name,
            link: val.link,
        }
    }
}
