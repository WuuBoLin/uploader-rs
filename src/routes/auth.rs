use askama::Template;
use axum::extract::Query;
use axum::http::StatusCode;
use axum::response::{Html, IntoResponse, Redirect, Response};
use axum::Json;
use serde::{Deserialize, Serialize};

use crate::auth::{AuthSession, Credentials};
use crate::templates::LoginTemplate;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NextUrl {
    next: Option<String>,
}

pub async fn login_page(auth: AuthSession) -> Response {
    if auth.user.is_some() {
        return Redirect::permanent("/admin").into_response();
    }

    Html(LoginTemplate {}.render().unwrap()).into_response()
}

pub async fn logout(
    mut auth_session: AuthSession,
    Query(NextUrl { next }): Query<NextUrl>,
) -> Result<Response, StatusCode> {
    auth_session
        .logout()
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    if let Some(ref next) = next {
        return Ok(Redirect::to(next).into_response());
    }

    Ok(StatusCode::OK.into_response())
}

pub async fn login(
    mut auth_session: AuthSession,
    Query(NextUrl { next }): Query<NextUrl>,
    Json(creds): Json<Credentials>,
) -> Result<Response, StatusCode> {
    let user = match auth_session.authenticate(creds.clone()).await {
        Ok(Some(user)) => user,
        Ok(None) => return Err(StatusCode::UNAUTHORIZED),
        Err(_) => return Err(StatusCode::INTERNAL_SERVER_ERROR),
    };

    auth_session
        .login(&user)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    if let Some(ref next) = next {
        return Ok(Redirect::to(next).into_response());
    }

    Ok(StatusCode::OK.into_response())
}
