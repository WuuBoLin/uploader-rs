use std::env;

use crate::auth::AuthSession;
use crate::auth::User;
use crate::server::ServerState;
use crate::templates::AdminTemplate;
use crate::templates::FileMeta;
use crate::templates::NotFoundTemplate;
use crate::templates::QuickLoginTemplate;

use askama::Template;
use axum::extract::Path;
use axum::extract::State;
use axum::http::StatusCode;
use axum::response::Html;
use tracing::info;

use super::api::File;

pub async fn admin_page(State(state): State<ServerState>) -> Result<Html<String>, StatusCode> {
    let files: Vec<File> = state
        .db
        .query("SELECT * FROM file ORDER BY date DESC")
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
        .take(0)
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let files = files
        .iter()
        .map(|f| FileMeta {
            name: f.name.clone(),
            date: f.date.to_rfc3339(),
            size: f.size.clone(),
            sharedate: f.date.to_rfc3339(),
            downloads: f.downloads,
        })
        .collect();

    Ok(Html(AdminTemplate { all_files: files }.render().unwrap()))
}

pub async fn quick_token(
    mut auth: AuthSession,
    State(state): State<ServerState>,
    Path(token): Path<String>,
) -> Result<Html<String>, (StatusCode, Html<String>)> {
    if token
        == env::var("QUICK_TOKEN")
            .map_err(|_| (StatusCode::NOT_IMPLEMENTED, Html(String::new())))?
    {
        let user: Option<User> = state
            .db
            .query("SELECT * FROM type::table($table) WHERE username = $username")
            .bind(("table", "user"))
            .bind(("username", "admin"))
            .await
            .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?
            .take(0)
            .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, Html(String::new())))?;

        let Some(user) = user else {
            return Err((StatusCode::NOT_IMPLEMENTED, Html(String::new())));
        };

        auth.login(&user).await.unwrap();
        info!("User {} Logged in WITH QUICK TOKEN", user.username);
        return Ok(Html(QuickLoginTemplate.render().unwrap()));
    }

    Err((
        StatusCode::UNAUTHORIZED,
        Html(NotFoundTemplate.render().unwrap()),
    ))
}
