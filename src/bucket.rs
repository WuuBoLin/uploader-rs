use std::time::Duration;

use aws_sdk_s3::{
    presigning::{PresignedRequest, PresigningConfig},
    types::{Delete, ObjectIdentifier},
    Client,
};
use tracing::info;

const BUCKET_NAME: &str = "dashthings";

pub enum UrlType {
    Direct,
    Preview,
}

pub enum PresignedMethod {
    Get(UrlType),
    Put,
}

pub async fn get_presigned_url(
    method: PresignedMethod,
    client: &Client,
    object: &str,
    expires_in: u64,
) -> anyhow::Result<PresignedRequest> {
    let expires_in = Duration::from_secs(expires_in);

    let presigned_request = match method {
        PresignedMethod::Get(url_type) => {
            let presigned_url = client
                .get_object()
                .bucket(BUCKET_NAME)
                .key(object)
                .response_content_type(get_content_type(object));

            match url_type {
                UrlType::Direct => presigned_url.response_content_disposition("attachment"),
                UrlType::Preview => presigned_url.response_content_disposition("inline"),
            }
            .presigned(PresigningConfig::expires_in(expires_in)?)
            .await?
        }
        PresignedMethod::Put => {
            client
                .put_object()
                .bucket(BUCKET_NAME)
                .key(object)
                .presigned(PresigningConfig::expires_in(expires_in)?)
                .await?
        }
    };

    info!(
        "Generated S3 presigned url: {}",
        presigned_request.uri().to_string()
    );

    Ok(presigned_request)
}

fn get_content_type(file_path: &str) -> String {
    mime_guess::from_ext(file_path.split('.').last().unwrap_or(file_path))
        .first_or_octet_stream()
        .to_string()
}

pub async fn delete(client: &Client, objects: &[impl ToString]) -> anyhow::Result<()> {
    let objects = objects
        .iter()
        .map(|o| {
            ObjectIdentifier::builder()
                .key(o.to_string())
                .build()
                .unwrap()
        })
        .collect();

    client
        .delete_objects()
        .bucket(BUCKET_NAME)
        .delete(Delete::builder().set_objects(Some(objects)).build()?)
        .send()
        .await?;

    Ok(())
}

pub async fn rename(client: &Client, object: &str, to: &str) -> anyhow::Result<()> {
    let mut source_bucket_and_object = String::new();
    source_bucket_and_object.push_str(BUCKET_NAME);
    source_bucket_and_object.push('/');
    source_bucket_and_object.push_str(object);

    client
        .copy_object()
        .copy_source(urlencoding::encode(&source_bucket_and_object))
        .bucket(BUCKET_NAME)
        .key(to)
        .send()
        .await?;

    delete(client, &[object]).await?;

    Ok(())
}
