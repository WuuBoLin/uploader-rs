mod auth;
mod bucket;
mod migration;
mod routes;
mod server;
mod templates;

use server::Server;
use tracing_subscriber::{
    filter::LevelFilter, fmt, prelude::__tracing_subscriber_SubscriberExt, reload,
    util::SubscriberInitExt,
};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let (filter, _) = reload::Layer::new(LevelFilter::INFO);
    tracing_subscriber::registry()
        .with(filter)
        .with(fmt::Layer::default())
        .init();

    dotenv::dotenv().ok();

    Server::launch().await?;

    Ok(())
}
