use askama::Template;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FileMeta {
    pub name: String,
    pub date: String,
    pub size: String,
    pub sharedate: String,
    pub downloads: usize,
}

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate {
    pub all_files: Vec<FileMeta>,
}

#[derive(Template)]
#[template(path = "login.html")]
pub struct LoginTemplate;

#[derive(Template)]
#[template(path = "admin.html")]
pub struct AdminTemplate {
    pub all_files: Vec<FileMeta>,
}

#[derive(Template)]
#[template(path = "quick.html")]
pub struct QuickLoginTemplate;

#[derive(Template)]
#[template(path = "404.html")]
pub struct NotFoundTemplate;

#[derive(Template)]
#[template(path = "viewer.html")]
pub struct PdfViewTemplate;

#[derive(Template)]
#[template(path = "link.html")]
pub struct LinkTemplate {
    pub filename: String,
    pub url: String,
    pub size: String,
    pub content_url: String,
}
