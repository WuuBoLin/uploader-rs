let uploading = 0;
let file_count = 0;
let uploaded_count = 0;
let sharebutton;
let linkbutton;
let linkstate;

function downloadURL(URL) {
  let a = document.createElement('a');
  a.href = URL;
  a.click();
}

function downloadFile(filename) {
  $.ajax({
      url: `/api/request/download`,
      method: "POST",
      data: JSON.stringify({ files: [filename] }),
      contentType: "application/json;charset=utf-8",
      success: function (res) {
        downloadURL(res[0].url);
      }
  });
}

function previewFile(filename) {
  return new Promise(function(resolve, reject) {
    $.ajax({
      url: `/api/request/download?display=true`,
      method: "POST",
      data: JSON.stringify({ files: [filename] }),
      contentType: "application/json;charset=utf-8",
      success: function (res) {
        resolve(res[0].url);
      },
      error: function (err) {
        reject(err);
      }
    });
  });
}

function openPreviewInAnotherTab(filename){
  previewFile(filename)
      .then(function(url) {
        if (checkPreviewable(filename) === 'pdf') {
          url = `/pdfview?file=${encodeURIComponent(url)}`
        }
        window.open(url);
      })
      .catch(function(err) {
        console.error('Error:', err);
      });
}

function init() {
  let domain = window.location.href.split('/')[2];
  $('#baseurl').text(domain + ' /');
}

// popup
function upload() {
  hideall();
  $('#upload-area').show();
  let x = document.getElementsByClassName("popup")[0];
  x.classList.add("popup--opened");
}

function popupoff() {
  if (uploading === 0) {
    let x = document.getElementsByClassName("popup")[0];
    x.classList.remove("popup--opened");
  }

  $('#savechange').off('click');
  $('#delfile').off('click');
  $('#rename').off('click');
}

function hideall() {
  $('#modfile-area').hide();
  $('#upload-area').hide();
  $('#multi-modfile-area').hide();
}

function modify(name) {
  sharebutton = 0;
  linkbutton = 0;
  linkstate = 0;
  hideall();
  filestate(name);
  $('#link-input').hide();
  $('#modfile-area').show();
  $('#savechange').on('click', function () { save(name); });
  $('#delfile').on('click', function () { delFile(name); });
  $('#rename').on('click', function () { rename(name); });
  $('#modfile-name').html(name);
  let x = document.getElementsByClassName("popup")[0];
  x.classList.add("popup--opened");
}

function multimodify() {
  hideall();
  $('#multi-modfile-area').show();
  $('#multi-Share-check').prop('checked', false);
  $('#multi-modfile-name').html(selected.length + " Files");
  let x = document.getElementsByClassName("popup")[0];
  x.classList.add("popup--opened");
}

function downloadMulti() {
  selecting = 0;
  $('#xbtn').prop('disabled', true);
  $('#edit-options-text').text("Preparing Download...");
  $('#download-btn-text').hide();
  $('#download-btn-loading').show();
  $('#downloadzip-btn').prop('disabled', true);
  $('#multimodify-btn').addClass('select-edit-hide');
  let data = JSON.stringify({ files: selected });
  $.ajax({
    url: "/api/request/download",
    method: "POST",
    data: data,
    contentType: "application/json;charset=utf-8",
    success: function (res) {
      for (let i = 0; i < res.length; i++) {
        downloadURL(res[i].url);
      }
    }
  }).then(function () {
    cancelselect();
    setTimeout(function () {
      $('#xbtn').prop('disabled', false);
      $('#download-btn-text').show();
      $('#download-btn-loading').hide();
      $('#downloadzip-btn').prop('disabled', false);
      $('#multimodify-btn').removeClass('select-edit-hide');
    }, 280);
  });
}

function delFile(filename) {
  Swal.fire({
    title: 'Delete?',
    text: "You won't be able to revert this!",
    // icon: 'question',
    showCancelButton: true,
    confirmButtonColor: '#d33',
    cancelButtonColor: '#51597e',
    confirmButtonText: 'Yes',
    allowEnterKey: false,
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url: `/api/files/${filename}`,
        method: "DELETE",
        dataType: 'text',
        success: function () {
            location.reload();
        },
        error: function (error) {
          console.log(error);
          Swal.fire({
            title: "Error",
            text: "Something went wrong, please try again later.",
            // icon: "error",
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: true,
            confirmButtonText: "Reload Page",
            confirmButtonColor: "#546ad8",
          }).then((result) => {
            if (result.isConfirmed) {
              location.reload();
            }
          }
          );
        }
      });
    }
  })
}

// ui

let upload_files_list = [];

function add_file(id, file) {
  upload_files_list.push(file.name);

  file_count++;
  let template = $('#files-template').text();
  template = template.replace('%%filename%%', file.name);
  template = $(template);
  template.prop('id', 'file-' + id);

  $('#uploading-list').append(template[0]);
}

function clear_files() {
  upload_files_list = [];
  file_count = 0;
  $('#uploading-list').empty();
}

function file_progress(id, percent) {
  let file = $('#file-' + id);
  let progress = file.find('#progressbar');
  let percent_text = file.find('#percent_text');
  progress.css('width', percent + '%');
  percent_text.text(percent + '%');
  if (percent === 100) {
    progress.css('backgroundColor', '#546ad8');
  }
}

function filestate(filename) {
  $.ajax({
    url: `/api/files/${filename}`,
    method: "GET",
    dataType: 'text',
    success: function (res) {
      let data = JSON.parse(res);
      sharebutton = !!data.sharedate;
      linkstate = data.link;
      if (sharebutton) {
        $('#Share-check').prop('checked', true);
      } else {
        $('#Share-check').prop('checked', false);
      }
      if (linkstate) {
        linkbutton = 1;
        $('#Share-link-check').prop('checked', true);
        $('#link-input').show();
        $('#shortlink').val(linkstate);
      } else {
        linkbutton = 0;
        $('#Share-link-check').prop('checked', false);
        $('#link-input').hide();
        $('#shortlink').val("");
      }
    }
  });
}

function save(filename) {
  let shortlink = $('#shortlink').val();
  let share = $('#Share-check').is(":checked");
  let link = $('#Share-link-check').is(":checked");
  if (sharebutton != share) {
    $.ajax({
      url: "/api/files",
      method: "POST",
      dataType: 'text',
      data: JSON.stringify({ files: [filename], share: share }),
      contentType: "application/json;charset=utf-8",
      success: function (res) {
          popupoff();
      }
    });
  }
  if (link) {
    if (linkstate != shortlink) {
      if (shortlink === "") {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'You must enter a shortlink!',
        })
      }

      fetch("/api/files", {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8"
        },
        body: JSON.stringify({ files: [filename], link: shortlink })
      })
          .then(response => {
            if (!response.ok) {
              return response.text().then(error => {
                if (error === "LINK_EXIST") {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'This shortlink already in use!',
                  });
                } else if (error === "LINK_ILLEGAL") {
                  $('#shortlink').val("");
                  Swal.fire({
                    icon: 'error',
                    title: 'Wait!',
                    text: "The shortlink can't contain special characters or spaces",
                  });
                }
                throw new Error(error);
              });
            }
            popupoff();
          })
          .catch(error => {
            console.error('Error:', error);
          });
    }
    else {
      popupoff();
    }
  }
  else {
    $.ajax({
      url: "/api/files",
      method: "POST",
      dataType: 'text',
      data: JSON.stringify({ files: [filename], link: "" }),
      contentType: "application/json;charset=utf-8",
      success: function (res) {
          popupoff();
      }
    });
  }
}

let selecting = 0;
let functioning = 0;
function selectfile() {
  selecting = 1;
  functioning = 1;
  topiconhide();
  $('.edit-options').show();
  $('.modfile-icon').addClass('hide');
  $('.file-card').addClass('file-card-disable');
  $('.file').addClass('file-select');
  $('.file-list').addClass('file-list-editing');
}

let selected = [];
function cancelselect() {
  $('.modfile-icon').removeClass('hide');
  $('.file').removeClass('file-select');
  $('.file').removeClass('file-selected');
  $('.file-card').removeClass('file-card-disable');
  $('.file-card').removeClass('file-card-selected');
  $('.edit-options').removeClass('edit-options-open');
  $('.file-list').removeClass('file-list-editing');
  topiconshow();

  selecting = 0;
  functioning = 0;
  selected = [];

  setTimeout(function () {
    $('.edit-options').hide();
  }, 280);
}

function topiconhide() {
  $('#ani-button').addClass('cancel-select');
  $('.opt-icons').addClass('hide');
}

function topiconshow() {
  $('#ani-button').removeClass('cancel-select');
  $('.opt-icons').removeClass('hide');
}

function func_button() {
  if (functioning === 0) {
    upload();
  } else {
    cancelselect();
    searchclose();
  }
}

function select(fileID) {
  if (selecting === 1) {

    let filename = $('#' + fileID + "-name").text();
    if (selected.includes(filename)) {
      selected.splice(selected.indexOf(filename), 1);
      multi_select_ui(fileID, 1);
    }
    else {
      selected.push(filename);
      multi_select_ui(fileID, 0);
    }
  }


}

function multi_select_ui(fileID, state) {
  if (selected.length > 0) {
    $('#edit-options-text').text(selected.length + " Files selected");
    $('.edit-options').addClass('edit-options-open');
  } else {
    $('.edit-options').removeClass('edit-options-open');
  }

  if (state === 1) {
    $("#" + fileID).removeClass('file-selected');
    $("#" + fileID + "-card").removeClass('file-card-selected');
    $("#" + fileID + "-card").addClass('file-card-disable');
  }
  else {
    $("#" + fileID).addClass('file-selected');
    $("#" + fileID + "-card").removeClass('file-card-disable');
    $("#" + fileID + "-card").addClass('file-card-selected');
  }
}

function multidelete() {
  Swal.fire({
    title: "Are you sure?",
    text: "You will delete " + selected.length + " files",
    // icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#7e403e",
    confirmButtonText: "Delete",
    allowEnterKey: false,
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url: "/api/files",
        method: "DELETE",
        dataType: 'text',
        data: JSON.stringify({ files: selected }),
        contentType: "application/json;charset=utf-8",
        success: function () {
            location.reload();
        }
      });
    }
  })
}

function multishare() {
  let share = $('#multi-Share-check').is(":checked");

  $.ajax({
    url: "/api/files",
    method: "POST",
    dataType: 'text',
    data: JSON.stringify({ files: selected, share: share }),
    contentType: "application/json;charset=utf-8",
    success: function (res) {
      popupoff();
      cancelselect();
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: 'Change applied!',
        })
    }
  });
}


// toggle options area and outside click to close
function toggleoptions() {
  $('.upload-options-area').toggleClass('upload-options-area--open');
}

$(document).mouseup(function (e) {
  let container = $(".upload-options-area");
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    container.removeClass('upload-options-area--open');
  }
});


//rename
function rename(file) {
  return Swal.fire({
    title: 'Rename',
    text: "Enter a new name for " + file + ":",
    input: 'text',
    inputValue: file,
    inputAttributes: {
      autocapitalize: 'off',
      id: 'rename-input'
    },
    showCancelButton: true,
    confirmButtonText: 'Rename',
    showLoaderOnConfirm: true,
    didOpen: function () {
      const dotIndex = $('#rename-input').val().lastIndexOf('.')
      $('#rename-input')[0].setSelectionRange(0, dotIndex)
      $('#rename-input').focus()

    },
    preConfirm: (newname) => {
      if (newname === "") {
        Swal.showValidationMessage(
          `You must enter a name`
        )
      }
      else if (newname === file) {
        Swal.showValidationMessage(
          `You must enter a different name`
        )
      }
      else if (!/^[\p{L}\p{N}\-._\s]+$/u.test(newname)) {
        Swal.showValidationMessage(
          `The name can't contain special characters`
        )
      }
      else {
        return fetch("/api/files", {
          method: "POST",
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
          },
          body: JSON.stringify({
            files: [file],
            name: newname
          })
        })
            .then(response => {
              if (!response.ok) {
                return response.text().then(errorData => {
                  throw new Error(errorData);
                });
              }
              return response.text();
            })
            .then(() => {
              location.reload();
            })
            .catch(error => {
              console.log(error.message)
              if (error.message === "FILENAME_EXIST") {
                Swal.showValidationMessage(`This name is already in use`);
              } else if (error.message === "FILENAME_ILLEGAL") {
                Swal.showValidationMessage(`The name can't contain special characters or spaces`);
              } else {
                console.error(error);
              }
            });
      }
    }
  })
}

//preview

function checkPreviewable(filename) {
  let ext = filename.split('.').pop().toLowerCase();

  if (filename.indexOf('.') === -1) {
    return false;
  }

  if (!ext) {
    return false;
  }

  let image = ['bmp', 'gif', 'ico', 'jpeg', 'jpg', 'png', 'svg', 'tiff', 'webp'];
  let text = ['txt', 'md', 'log', 'csv', 'tsv', 'tab', 'json', 'xml', 'html', 'htm', 'css', 'js', 'jsx', 'php', 'rb', 'py', 'c', 'cpp', 'h', 'hpp', 'java', 'pl', 'sh', 'bat', 'ps1', 'sql', 'r', 'yaml', 'yml', 'ini', 'env', 'cmd'];

  if (ext === 'pdf') {
    return ext;
  }

  if (image.indexOf(ext) != -1) {
    return 'image';
  }

  if (text.indexOf(ext) != -1) {
    return 'text';
  }

  return false;
}

async function preview(filename) {
  let link;
  let content;

  let filetype = checkPreviewable(filename);

  if (!filetype) {
    content =
      `<div class="preview-info">
          <a class="preview-notavailable">Preview not available</a>
          <button class="button preview-download" onclick="downloadFile('${filename}', false)">Download</button>
      </div>`;
  }

  if (filetype === 'pdf') {
    link = encodeURIComponent(await previewFile(filename));
    link = `/pdfview?file=${link}`
    content = `<iframe class="preview-iframe" src="${link}"></iframe>`;
  }

  if (filetype === 'image') {
    link = await previewFile(filename);
    content = `<img class="preview-img" src="${link}">`;
  }

  if (filetype === 'text') {
    link = await previewFile(filename);
    let text = "";
    $.ajax({
      url: link,
      async: false,
      success: function (data) {
        text = data;
      }
    });

    content = `<textarea readonly class="preview-text">${text}</textarea>`;
  }

  let template = $('#file-prev').text();
  template = template.replaceAll('%filename%', filename);
  template = template.replaceAll('%preview-content%', content);
  template = template.replaceAll('%preview-link%', link);

  template = $(template);

  //disable scroll
  $('body').css('overflow', 'hidden');

  $('#preview-area').append(template);

  //for animation
  setTimeout(function () {
    template.addClass('popup--opened');
  }, 1);
}

function previewoff() {
  //enable scroll
  $('body').css('overflow', 'auto');

  $('#preview').removeClass('popup--opened');
  setTimeout(function () {
    $('#preview').remove();
  }, 200);
}

function searchopen() {
  topiconhide();
  functioning = 1;
  $('#search').addClass('search-open');
  $('#file-list').addClass('file-list-out');
  $('#search-input').focus();
  $('.opt-icons').addClass('hide');
  $('#xbtn').removeClass('top-icon-hide');
}

function searchclose() {
  $('#search').removeClass('search-open');
  $('#file-list').removeClass('file-list-out');
  $('#search-input').val('');
  $('.file').show();
  $('.no-files').hide();
  $('#clstext').removeClass('clstext-show');
  topiconshow();
}

function clearsearchtext() {
  $('#search-input').val('');
  $('#clstext').removeClass('clstext-show');
  $('.file').show();
  $('.no-files').hide();
}

function search(searchString) {
  const keys = Object.keys(files_list);
  const filteredKeys = keys.filter(key => key.toLowerCase().includes(searchString));
  const result = [];
  filteredKeys.forEach(key => {
    result.push(files_list[key]);
  }
  );

  $('.file').hide();

  if (result.length === 0) {
    $('.no-files').show();
  }
  else {
    $('.no-files').hide();
    result.forEach(file => {
      $('#' + file).show();
    }
    );
  }

}

$(document).ready(function () {
  $('#search-input').on('input', function (event) {
    let inputValue = $(this).val().toLowerCase();
    search(inputValue);
    if (inputValue.length > 0) {
      $('#clstext').addClass('clstext-show');
    }
    else {
      $('#clstext').removeClass('clstext-show');
    }
  });
});
